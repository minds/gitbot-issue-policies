FROM ruby:2.7

RUN gem install gitlab-triage

COPY .triage-policies.yml .triage-policies.yml 

CMD ["sh","-c","gitlab-triage --source-id minds --source groups --token=$GITLAB_ACCESS_TOKEN"]