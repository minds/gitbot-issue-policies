<!---
Please read this!

The Definition of Done (DoD) can be found at https://developers.minds.com/docs/handbook/how-we-work/#definition-of-done
--->

### Ticket(s) / Related Merge Requests

<!---
Any issue cards for the merge request, and related merge requests/dependencies

NOTE: include in the title of the MR and ensure you are prefix the repository if the issue differs, eg. minds#12
-->


### Summary of Changes

<!---
Summarize the changes made concisely
--->


### Testing Considerations

<!---
Share which areas to focus on while testing this MR
--->


### Deployment Considerations
<!---
Detail if there is anything required or to be monitored for the deployment
--->


### Regression Scope

<!---
Can this change cause another regression issue? Which areas to focus on for regression testing?
--->


### Platform Affected (web, mobile, etc)

<!---
Which platforms are affected by this change? Web, Mobile, or Both?
--->


### Developer Testing Completed

<!---
What has been tested already?
--->


### Screenshots / Screen Recording

<!---
Required for all merge requests
--->


### Does this impact
- [ ] Localization
- [ ] Dark/light mode
- [ ] Guest mode

### Definition of Done Checklist

- [ ] The Acceptance Criteria has been met
- [ ] Code is tested: Testing includes unit/spec, E2E/automated and manual testing
- [ ] Merge requests description has been filled out


/label mr-has-template
