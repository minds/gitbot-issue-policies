<!---
Please read this!

The Definition of Ready (DoR) can be found at https://developers.minds.com/docs/handbook/how-we-work/#definition-of-ready-dor

Before opening a new issue, make sure to search for keywords in the issues
filtered by the "Type::Feature" label and verify the feature you're about to submit isn't a duplicate.
--->

## Goal

<!---
Clearly outline the goal of the issue and why we are doing it
--->

- To serve as a template and a place for user research insights, user testing scripts, interview notes and more. The goal would be that for each design issue that requires a considerable amount of product thinking, user research or testing, folks could duplicate this template, make it part of the Gitlab issue / link it to the issue being worked on by making it a separate Gitlab issue. Folks could then put in their research proposal, user interview notes, script, and any insights they gather and uncover for all teammates to get visibility.

## What needs to be done

<!---
Clearly detail what needs to be done to achieve the goal set out above
--->

- Markdown document of research proposal template, then sent to Mark for making it into a Gitlab template.


## QA

<!---
Consideration is given to how the issue will be demoed and tested prior to being deployed
--->


## UX/Design

<!---
Sufficient wireframes have been presented and understood by the development team. Full mockups are not required and re-usable components are encouraged.
--->


Asset as md file - 
[Minds_Research_proposal_template.md](/uploads/b06642de029d8e65118b07a66f71e2b9/Minds_Research_proposal_template.md)

## Personas

<!---
Who does this user impact and why (optional)
--->

- link to other personas here

## Experiments

<!---
Experiments are not required, but it should be made clear if one is expected. If experiments are required, the hypothesis should be outlined.
--->


## Acceptance Criteria

<!--
Everyone should be able to understand what is expected to be delivered
--->

- [ ] collect feedback from product team
- [ ] collect feedback from Mark
- [ ] Prepare asset into .md file. 
- [ ] Make into template

## Definition of Ready Checklist

- [ ] Definition Of Done (DoD)
- [ ] Acceptance criteria
- [ ] Weighted
- [ ] QA
- [ ] UX/Design
- [ ] Personas
- [ ] Experiments